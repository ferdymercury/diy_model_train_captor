# DIY_model_train_captor

based on schema found here : https://www.youtube.com/watch?v=mrb9soMySXE at chrono 19:30 & keeping it in sizing at chrono 23:00

# Video 

resulted PCB : https://youtu.be/zvKNefB6FiE

# Files 

[layers](MTC.pdf)

[project & gerber files](MTC/diy_)

[exports](MTC/exports)

![image1](00.png)

![image2](01.png)

![image3](02.png)

![image4](03.png)

# Refs

used [Kicad](https://kicad-pcb.org/) v5.1.6
